import { h } from 'preact'
import React from 'react'
import ReactDOM from 'react-dom'
import { motion } from 'framer-motion'

function App() {
  return (
    <SimpleCard />
  );
}

function SimpleCard()
{
  return (
    <motion.div className="card" {...getAnimationProps()}>
      <h3 className="card__title">
        Simple Title
      </h3>
    </motion.div>
  );
}

function getAnimationProps()
{
  return {
    transition: {
      ease: 'anticipate'
    },
    whileTap: {
      scale: 0.85,
      rotate: 90
    }
  };
}

ReactDOM.render(<App />, document.getElementById('root'));