# Framer Motion Example with Parcel and Preact

## Demo at https://hungry-borg-d27496.netlify.app

[![Netlify Status](https://api.netlify.com/api/v1/badges/ef37805b-3340-4296-9b05-68890e7864f3/deploy-status)](https://app.netlify.com/sites/hungry-borg-d27496/deploys)

Example code based on https://codepen.io/hussard/full/abzgjmO

```bash
npm i
parcel index.html || npx parcel index.html
```
